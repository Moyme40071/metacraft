#Calculate max energy intake.
scoreboard players operation @s MaxEnergyIntake = @s ObjectMaxEnergy
scoreboard players operation @s MaxEnergyIntake -= @s ObjectEnergy
#Get limiting IO
execute if score @s ObjectEnergyIO <= @e[tag=CurrentGenerator,limit=1] ObjectEnergyIO run scoreboard players operation @s LimitingIO = @s ObjectEnergyIO
execute unless score @s ObjectEnergyIO <= @e[tag=CurrentGenerator,limit=1] ObjectEnergyIO run scoreboard players operation @s LimitingIO = @e[tag=CurrentGenerator,limit=1] ObjectEnergyIO
#Enough energy in generator, enough space in acceptor, transfer full possible IO
execute as @s if score @s MaxEnergyIntake >= @s LimitingIO if score @e[tag=CurrentGenerator,limit=1] ObjectEnergy >= @s LimitingIO run function power:object-draw-max-io-from-curr-generator
#Less energy in generator than limiting IO but enough space left in target.
execute as @s if score @s MaxEnergyIntake >= @e[tag=CurrentGenerator,limit=1] ObjectEnergy if score @e[tag=CurrentGenerator,limit=1] ObjectEnergy < @s LimitingIO if score @s MaxEnergyIntake >= @e[tag=CurrentGenerator,limit=1] ObjectEnergy run function power:draw-left-power-from-curr-generator
#Not enough space in target, enough energy to fully fill space
execute as @s if score @s MaxEnergyIntake < @e[tag=CurrentGenerator,limit=1] ObjectEnergy if score @e[tag=CurrentGenerator,limit=1] if @s MaxEnergyIntake >= @s LimitingIO run function power:draw-left-power-from-curr-generator
