execute as @s[tag=South] at @s positioned ~ ~ ~1 unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectNorth] at @s positioned ~ ~-0.768 ~0.31 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=North] at @s positioned ~ ~ ~-1 unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectSouth] at @s positioned ~ ~-0.768 ~-0.31 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=East] at @s positioned ~1 ~ ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectWest] at @s positioned ~0.31 ~-0.768 ~ run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=West] at @s positioned ~-1 ~ ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectEast] at @s positioned ~-0.31 ~-0.768 ~ run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=Up] at @s positioned ~ ~1 ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectDown] at @s positioned ~ ~-0.66 ~-0.205 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=Down] at @s positioned ~ ~-1 ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectUp] at @s positioned ~ ~-1.283 ~-0.205 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]

execute as @s[tag=South] at @s positioned ~ ~ ~1 unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectNorth] at @s run tag @s remove South
execute as @s[tag=North] at @s positioned ~ ~ ~-1 unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectSouth] at @s run tag @s remove North
execute as @s[tag=East] at @s positioned ~1 ~ ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectWest] at @s run tag @s remove East
execute as @s[tag=West] at @s positioned ~-1 ~ ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectEast] at @s run tag @s remove West
execute as @s[tag=Up] at @s positioned ~ ~1 ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectDown] at @s run tag @s remove Up
execute as @s[tag=Down] at @s positioned ~ ~-1 ~ unless entity @e[distance=..0,sort=nearest,limit=1,tag=ConnectUp] at @s run tag @s remove Down