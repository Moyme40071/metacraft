execute as @s[scores={Slot0=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot1=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot2=-1}] run scoreboard players add @s TestEmptySlot 1

execute as @s[scores={Slot3=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot4=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot5=-1}] run scoreboard players add @s TestEmptySlot 1

execute as @s[scores={Slot6=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot7=-1}] run scoreboard players add @s TestEmptySlot 1
execute as @s[scores={Slot8=-1}] run scoreboard players add @s TestEmptySlot 1

execute if block ~ ~ ~ dispenser{Items:[{Slot:0b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:1b,id:"minecraft:redstone"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:2b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1

execute if block ~ ~ ~ dispenser{Items:[{Slot:3b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:4b,id:"minecraft:redstone"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:5b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1

execute if block ~ ~ ~ dispenser{Items:[{Slot:6b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:7b,id:"minecraft:redstone"}]} run scoreboard players add @s TestFullSlot 1
execute if block ~ ~ ~ dispenser{Items:[{Slot:8b,id:"minecraft:iron_ingot"}]} run scoreboard players add @s TestFullSlot 1

execute as @s[scores={TestFullSlot=3,TestEmptySlot=6}] run setblock ~ ~ ~ minecraft:dispenser[facing=up]
execute as @s[scores={TestFullSlot=3,TestEmptySlot=6}] run data merge block ~ ~ ~ {Lock:"§"}
scoreboard players set @s[scores={TestFullSlot=3,TestEmptySlot=6}] CraftingTime 20
scoreboard players operation @s[scores={TestFullSlot=3,TestEmptySlot=6}] CraftingTime *= @s[scores={TestFullSlot=3,TestEmptySlot=6}] LowestItemCount
scoreboard players set @s TestFullSlot 0
scoreboard players set @s TestEmptySlot 0
scoreboard players set @s CraftingOutput 1