scoreboard objectives add ItemCount dummy
scoreboard objectives add LowestItemCount dummy
scoreboard objectives add TestSlot dummy
scoreboard objectives add Slot0 dummy
scoreboard objectives add Slot1 dummy
scoreboard objectives add Slot2 dummy
scoreboard objectives add Slot3 dummy
scoreboard objectives add Slot4 dummy
scoreboard objectives add Slot5 dummy
scoreboard objectives add Slot6 dummy
scoreboard objectives add Slot7 dummy
scoreboard objectives add Slot8 dummy

scoreboard players set @s LowestItemCount 128
scoreboard players set @s Slot0 -1
scoreboard players set @s Slot1 -1
scoreboard players set @s Slot2 -1
scoreboard players set @s Slot3 -1
scoreboard players set @s Slot4 -1
scoreboard players set @s Slot5 -1
scoreboard players set @s Slot6 -1
scoreboard players set @s Slot7 -1
scoreboard players set @s Slot8 -1

execute store result score @s ItemCount run data get block ~ ~ ~ Items[0].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[0].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=0}] run scoreboard players operation @s Slot0 = @s ItemCount
execute as @s[scores={TestSlot=1}] run scoreboard players operation @s Slot1 = @s ItemCount
execute as @s[scores={TestSlot=2}] run scoreboard players operation @s Slot2 = @s ItemCount
execute as @s[scores={TestSlot=3}] run scoreboard players operation @s Slot3 = @s ItemCount
execute as @s[scores={TestSlot=4}] run scoreboard players operation @s Slot4 = @s ItemCount
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[1].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[1].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=1}] run scoreboard players operation @s Slot1 = @s ItemCount
execute as @s[scores={TestSlot=2}] run scoreboard players operation @s Slot2 = @s ItemCount
execute as @s[scores={TestSlot=3}] run scoreboard players operation @s Slot3 = @s ItemCount
execute as @s[scores={TestSlot=4}] run scoreboard players operation @s Slot4 = @s ItemCount
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[2].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[2].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=2}] run scoreboard players operation @s Slot2 = @s ItemCount
execute as @s[scores={TestSlot=3}] run scoreboard players operation @s Slot3 = @s ItemCount
execute as @s[scores={TestSlot=4}] run scoreboard players operation @s Slot4 = @s ItemCount
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[3].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[3].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=3}] run scoreboard players operation @s Slot3 = @s ItemCount
execute as @s[scores={TestSlot=4}] run scoreboard players operation @s Slot4 = @s ItemCount
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[4].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[4].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=4}] run scoreboard players operation @s Slot4 = @s ItemCount
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[5].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[5].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=5}] run scoreboard players operation @s Slot5 = @s ItemCount
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[6].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[6].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=6}] run scoreboard players operation @s Slot6 = @s ItemCount
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[7].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[7].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=7}] run scoreboard players operation @s Slot7 = @s ItemCount
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute store result score @s ItemCount run data get block ~ ~ ~ Items[8].Count 1
execute store result score @s TestSlot run data get block ~ ~ ~ Items[8].Slot 1
execute as @s[scores={ItemCount=1..}] run scoreboard players operation @s LowestItemCount < @s ItemCount
execute as @s[scores={ItemCount=0}] at @s run scoreboard players set @s ItemCount -1
execute as @s[scores={TestSlot=8}] run scoreboard players operation @s Slot8 = @s ItemCount

execute as @s[scores={Slot0=1..}] run scoreboard players operation @s Slot0 -= @s LowestItemCount
execute as @s[scores={Slot1=1..}] run scoreboard players operation @s Slot1 -= @s LowestItemCount
execute as @s[scores={Slot2=1..}] run scoreboard players operation @s Slot2 -= @s LowestItemCount
execute as @s[scores={Slot3=1..}] run scoreboard players operation @s Slot3 -= @s LowestItemCount
execute as @s[scores={Slot4=1..}] run scoreboard players operation @s Slot4 -= @s LowestItemCount
execute as @s[scores={Slot5=1..}] run scoreboard players operation @s Slot5 -= @s LowestItemCount
execute as @s[scores={Slot6=1..}] run scoreboard players operation @s Slot6 -= @s LowestItemCount
execute as @s[scores={Slot7=1..}] run scoreboard players operation @s Slot7 -= @s LowestItemCount
execute as @s[scores={Slot8=1..}] run scoreboard players operation @s Slot8 -= @s LowestItemCount

function metacraft:blocks/crafting-machine/recipes/recipes