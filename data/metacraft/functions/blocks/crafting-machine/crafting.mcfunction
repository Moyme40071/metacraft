playsound minecraft:entity.zombie.attack_iron_door block @a[distance=..8] ~ ~ ~ 0.02 0
scoreboard players remove @s CraftingTime 1
execute as @e[tag=CraftingMachine,scores={CraftingTime=8..}] at @s positioned ~ ~-0.92 ~ as @e[distance=..0,sort=nearest,limit=1,tag=CraftingMachineButton] at @s run teleport @s ~ ~ ~ ~1 ~
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s positioned ~ ~-0.92 ~ as @e[distance=..0,sort=nearest,limit=1,tag=CraftingMachineButton] at @s run data merge entity @s {HandItems:[{id:"minecraft:stone",Count:1b,tag:{display:{Lore:["{\"text\":\"StartCrafting\"}"]}}},{}]}
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s positioned ~ ~-0.92 ~ as @e[distance=..0,sort=nearest,limit=1,tag=CraftingMachineButton] at @s run teleport @s ~ ~0.15 ~ 0 0
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s run particle minecraft:lava ~ ~0.5 ~ 0 0 0 0 100 normal @a
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s run playsound minecraft:entity.iron_golem.hurt block @a ~ ~1 ~ 1 0.4
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s run tag @s remove crafting
execute as @e[tag=CraftingMachine,scores={CraftingTime=1}] at @s run function metacraft:blocks/crafting-machine/recipes/output
