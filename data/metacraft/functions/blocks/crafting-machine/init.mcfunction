setblock ~ ~ ~ minecraft:dispenser[facing=up]{CustomName: '{"text":"Crafting Machine"}'}
summon minecraft:armor_stand ~ ~ ~ {Tags:["CraftingMachine","ConnectSouth","ConnectNorth","ConnectEast","ConnectWest","ConnectUp","ConnectDown"],NoBasePlate:1b,Invulnerable:1b,Invisible:1b,NoGravity:1b,Small:1b,Marker:1b}
summon minecraft:armor_stand ~ ~-0.77 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,Tags:["CraftingMachineButton"],Pose:{RightArm:[0f,270f,0f],Head:[180f,0f,0f]},DisabledSlots:4144703,HandItems:[{id:"minecraft:stone",Count:1b,tag:{display:{Lore:["{\"text\":\"StartCrafting\"}"]}}},{}],ArmorItems:[{},{},{},{id:"minecraft:chiseled_stone_bricks",Count:1b}]}
scoreboard objectives add CraftingTest dummy
scoreboard objectives add CraftingTime dummy
scoreboard objectives add CraftingOutput dummy
scoreboard objectives add TestEmptySlot dummy
scoreboard objectives add TestFullSlot dummy