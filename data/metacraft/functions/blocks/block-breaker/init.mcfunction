summon armor_stand ~ ~ ~ {Tags: ["block_breaker"]}
scoreboard players set @s loses_energy 200
scoreboard players set @s energy 0

summon armor_stand ~ ~-1 ~ {Tags:["block_breaker,deko"],Invisible:1b,NoGravity:1b,ShowArms:1b,ArmorItems:[{},{},{},{}],HandItems:[{id:"diamond_pickaxe",Count:1b},{}],Pose:{RightArm:[0f,0f,0f]}}

execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=north] run data merge entity @s {Rotation:[180f]}
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=south] run data merge entity @s {Rotation:[0f]}
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=east] run data merge entity @s {Rotation:[-90f]}
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=west] run data merge entity @s {Rotation:[90f]}
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=north] align xyz positioned ~0.5 ~0.5 ~0.5 run teleport @s ~0.15 ~ ~
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=south] align xyz positioned ~0.5 ~0.5 ~0.5 run teleport @s ~-0.15 ~ ~
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=east] align xyz positioned ~0.5 ~0.5 ~0.5 run teleport @s ~ ~ ~0.15
execute as @e[type=armor_stand] at @s if block ~ ~1 ~ minecraft:observer[facing=west] align xyz positioned ~0.5 ~0.5 ~0.5 run teleport @s ~ ~ ~-0.15