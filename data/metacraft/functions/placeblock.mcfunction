#Cable
execute as @a[scores={placePlayerHead=1..}] at @s run function metacraft:blocks/find-block
execute as @a[scores={minePlayerHead=1..}] at @s as @e[type=armor_stand,tag=Cable,distance=..8] at @s unless block ~ ~ ~ minecraft:player_head run function metacraft:blocks/cable/kill
execute as @a[gamemode=creative] at @s as @e[type=armor_stand,tag=Cable,distance=..8] at @s unless block ~ ~ ~ minecraft:player_head run function metacraft:blocks/cable/kill
scoreboard players set @a[scores={minePlayerHead=1..}] minePlayerHead 0
#Block Breaker
execute as @a[scores={ObserverPlace=1..}] at @s run function metacraft:blocks/find-block
scoreboard players set @a[scores={ObserverPlace=1..}] ObserverPlace 0